SET FOREIGN_KEY_CHECKS = 0;

DROP DATABASE IF EXISTS logdb;
CREATE DATABASE logdb CHARSET=utf8;
USE logdb;
SET NAMES 'utf8';

CREATE TABLE log (
    id                  bigint unsigned not null auto_increment unique primary key,
    facility            smallint     not null,
    severity            smallint     not null,
    timestamp           timestamp    not null default CURRENT_TIMESTAMP,
    hostname            varchar(64)  not null,
    application         varchar(32)  null default null,
    message             text         null default '',
    hmac                varchar(512) null default null
) ENGINE=myisam DEFAULT CHARSET=utf8;

CREATE TABLE facility (
    id                 smallint not null unique primary key,
    name               varchar(16) not null unique key
) ENGINE=myisam DEFAULT CHARSET=utf8;

INSERT INTO facility (id, name) VALUES (1, "kern");
INSERT INTO facility (id, name) VALUES (2, "user");
INSERT INTO facility (id, name) VALUES (3, "mail");
INSERT INTO facility (id, name) VALUES (4, "daemon");
INSERT INTO facility (id, name) VALUES (5, "auth");
INSERT INTO facility (id, name) VALUES (6, "syslog");
INSERT INTO facility (id, name) VALUES (7, "lpr");
INSERT INTO facility (id, name) VALUES (8, "news");
INSERT INTO facility (id, name) VALUES (9, "uucp");
INSERT INTO facility (id, name) VALUES (10, "cron");
INSERT INTO facility (id, name) VALUES (11, "authpriv");
INSERT INTO facility (id, name) VALUES (12, "ftp");
INSERT INTO facility (id, name) VALUES (13, "ntp");
INSERT INTO facility (id, name) VALUES (14, "audit");
INSERT INTO facility (id, name) VALUES (15, "alert");
INSERT INTO facility (id, name) VALUES (16, "cron2");
INSERT INTO facility (id, name) VALUES (17, "local0");
INSERT INTO facility (id, name) VALUES (18, "local1");
INSERT INTO facility (id, name) VALUES (19, "local2");
INSERT INTO facility (id, name) VALUES (20, "local3");
INSERT INTO facility (id, name) VALUES (21, "local4");
INSERT INTO facility (id, name) VALUES (22, "local5");
INSERT INTO facility (id, name) VALUES (23, "local6");
INSERT INTO facility (id, name) VALUES (24, "local7");


CREATE TABLE severity (
    id                 smallint not null unique primary key,
    name               varchar(16) not null unique key
) ENGINE=myisam DEFAULT CHARSET=utf8;

INSERT INTO severity (id, name) VALUES (1, "debug");
INSERT INTO severity (id, name) VALUES (2, "info");
INSERT INTO severity (id, name) VALUES (3, "notice");
INSERT INTO severity (id, name) VALUES (4, "warning");
INSERT INTO severity (id, name) VALUES (5, "error");
INSERT INTO severity (id, name) VALUES (6, "crit");
INSERT INTO severity (id, name) VALUES (7, "alert");
INSERT INTO severity (id, name) VALUES (8, "emerg");
INSERT INTO severity (id, name) VALUES (9, "nopri");
